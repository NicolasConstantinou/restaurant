json.array!(@drinks) do |drink|
  json.extract! drink, :id, :coke, :beer, :water
  json.url drink_url(drink, format: :json)
end
